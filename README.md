# React Native Web
- Boilerplate for React Native with Web served

Credit: https://aureliomerenda.medium.com/create-a-native-web-app-with-react-native-web-419acac86b82

‼‼‼
> It is better create from scratch, since the AppName from sources is always `ReactNativeWebApp`

## Dependencies

 - [React Native](https://github.com/facebook/react-native)
 - [React](https://github.com/facebook/react/)
 - [React Native for Web](https://github.com/necolas/react-native-web)
 - [Typescript](https://github.com/microsoft/TypeScript)
 - [Tailwind](https://github.com/tailwindlabs/tailwindcss)
 - [Tailwind React Native Classnames](https://github.com/jaredh159/tailwind-react-native-classnames)

## Base Steps

1. Init `react-native` `npx react-native init <project_name>`

### Based on plain `react-native` sources

2. Create `public/index.html` with content
```html
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="theme-color" content="#000000">
        <title>React App</title>
    </head>

    <body>
        <noscript>
            You need to enable JavaScript to run this app.
        </noscript>
        <div id="root"></div>
    </body>

</html>
```

3. Move files into `src`
```bash
mkdir src/
mv app.json src/
mv App.js src/
cp index.js src/
mv index.js index.native.js
```

4. Update `index.native.js`, for path changed
```js
- import App from './App';
- import {name as appName} from './app.json';
+ import App from './src/App';
+ import {name as appName} from './src/app.json';
```

5. Remove some components in `src/App.js`, mainly `react-native/Libraries` stuffs
```html
// Some examples...
<Header>
<ReloadInstructions>
<DebugInstructions>
<LearnMoreLinks>
```

6. Add `runApplication()` to `src/index.js`
```js
  AppRegistry.registerComponent(appName, () => App);
+ AppRegistry.runApplication(appName, {
+   rootTag: document.getElementById('root'),
+ });
```

7. Install packages
```bash
yarn add react-dom react-native-web
yarn add --dev react-scripts
```
### Maintain test

8. Update `__tests__/App-test.js`
```js
- import App from '../App';
+ import App from '../src/App';
```
9. Remove conflict packages from `package.json`
```json
-   "babel-jest": "^25.1.0",
-   "jest": "^25.1.0",
```
10. Reintall and test
```bash
rm -rf node_modules
yarn
yarn run test
```

### Add web platform

11. Update `package.json`
```json
  "scripts": {
+   "web": "react-scripts start",
  }
```

## Scripts

**Run different platforms**

```bash
yarn run ios
yarn run android
yarn run web
```

**And common scripts from `react-native` package**
