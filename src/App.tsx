import React from 'react';
import type {ReactNode} from 'react'; // eslint-disable-line @typescript-eslint/no-unused-vars
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Text} from 'react-native';
import {screenRoutes} from './config/screenConfigs';

const Stack = createNativeStackNavigator();

const App: () => ReactNode = () => {
  return (
    <NavigationContainer
      linking={screenRoutes.linking}
      fallback={<Text>Loading...</Text>}>
      <Stack.Navigator>
        {screenRoutes.components.map((v: any, k: any) => {
          return v.component ? (
            <Stack.Screen
              key={k}
              name={v.name}
              component={v.component}
              options={{headerShown: false}}
            />
          ) : null;
        })}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;