# Folder Structures

## Descriptions

```bash
assets/ # contains images and styling files
components/ # enhanced UI components & sections, e.g. Buttons, Inputs, Forms
  elements/
  layout/
    partials/
  sections/
    partials/
config/ # config and environment variables
hooks/
layouts/ # different of layouts for pages
middleware/
pages/ # routes with screens & view / screen pages
routes/ # original react routes, ignore
services/ # api services
utils/ # common functions and objects
```