import {
  TouchableHighlight,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';

const Clickable = ({children, ...props}: {children: any; [x: string]: any}) => {
  const {signChild = false, dimmed = false} = props;

  if (signChild) {
    // Wrapped sign child component only
    return (
      <TouchableWithoutFeedback {...props}>{children}</TouchableWithoutFeedback>
    );
  }

  if (dimmed) {
    // Wrapped with an `Animated.View`
    return <TouchableOpacity {...props}>{children}</TouchableOpacity>;
  }

  // Standard wrapper
  return <TouchableHighlight {...props}>{children}</TouchableHighlight>;
};

export default Clickable;