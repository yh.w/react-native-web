import screenUtil from '../utils/screenUtil';

export const appTitle =
  'App';

const routeScreens = [
  {
    name: 'Index',
    route: '',
  },
  {
    name: 'NotFound',
    route: '404',
  },
];

export const screenRoutes = screenUtil.createLinking(routeScreens);

const configs = {
  screenRoutes,
};

export default configs;
