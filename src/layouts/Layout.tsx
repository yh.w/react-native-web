import React from 'react'; // React is required for Screen, by native mobile
import {useColorScheme} from 'react-native';
import ViewWrapper from './ViewWrapper';
import Header from '../components/layout/Header';
import Footer from '../components/layout/Footer';

const Layout = ({
  children,
  ...props // eslint-disable-line @typescript-eslint/no-unused-vars
}: {
  children: any;
  [x: string]: any;
}) => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {};

  return (
    <ViewWrapper isDarkMode={isDarkMode} backgroundStyle={backgroundStyle}>
      <Header />
      {children}
      <Footer />
    </ViewWrapper>
  );
};

export default Layout;
