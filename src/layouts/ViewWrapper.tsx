import React from 'react'; // React is required for Screen, by native mobile
import {SafeAreaView, ScrollView, StatusBar} from 'react-native';

const ViewWrapper = ({
  children,
  ...props
}: {
  children: any;
  [x: string]: any;
}) => {
  const {hidden = false, isDarkMode = false, backgroundStyle = {}} = props;

  return (
    <SafeAreaView>
      <StatusBar
        animated={true}
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        hidden={hidden}
      />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        {children}
      </ScrollView>
    </SafeAreaView>
  );
};

export default ViewWrapper;