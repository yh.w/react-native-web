import IndexScreen from './screens/IndexScreen';
import NotFoundScreen from './screens/NotFoundScreen';

/**
 * List of avaliable screens
 * @property {string} any To map RouteScreen.name to component used
 * @see utils/screenUtil
 */
const screens: {[propertyName: string]: any} = {
  Index: IndexScreen,
  NotFound: NotFoundScreen,
};

export default screens;
