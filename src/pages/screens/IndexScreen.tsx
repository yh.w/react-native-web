import React, {useEffect}  from 'react'; // React is required for Screen, by native mobile
import {View, Button} from 'react-native';
import {appTitle as title} from '../../config/screenConfigs';
import Layout from '../../layouts/Layout';

const IndexScreen = ({navigation}: {navigation: any}) => {
  useEffect(() => {
    navigation.setOptions({
      title: title,
    });
  }, [navigation]);

  return (
    <Layout>
      <View>
        <Button
          title="404"
          onPress={() => navigation.navigate('NotFound', {status: '404'})}
        />
      </View>
    </Layout>
  );
};

export default IndexScreen;
