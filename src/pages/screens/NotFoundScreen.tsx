import React, {useEffect}  from 'react'; // React is required for Screen, by native mobile
import {View, Text, Button} from 'react-native';
import {appTitle as title} from '../../config/screenConfigs';
import Layout from '../../layouts/Layout';

const NotFoundScreen = ({navigation}: {navigation: any}) => {
  useEffect(() => {
    navigation.setOptions({
      title: title,
    });
  }, [navigation]);

  return (
    <Layout>
      <View>
        <Text>404</Text>
        <Button title="Back" onPress={() => navigation.navigate('Index')} />
      </View>
    </Layout>
  );
};

export default NotFoundScreen;
