/* eslint-disable react-hooks/rules-of-hooks */
import {useNavigation} from '@react-navigation/native';

const navi = () => {
  type Nav = {
    navigate: (value: string, params: any) => void;
  };
  const navigation = useNavigation<Nav>();

  return navigation;
};

const naviUtil = {
  navi,
};

export default naviUtil;
