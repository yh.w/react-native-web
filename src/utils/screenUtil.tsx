import screenList from '../pages/screens';

type RouteScreen = {
  name: string;
  route: string;
};

/**
 * Return NavigationContainer linking values
 * @param {array} routeScreens config parsing into NavigationContainer linking config.screens
 * @param {array} prefixes for NavigationContainer linking
 * @return {object} object consists `linking`, `components` contain all screens
 */
const createLinking = (
  routeScreens: RouteScreen[] = [],
  prefixes: string[] = [],
) => {
  // Set default
  if (routeScreens.length < 1) {
    routeScreens.push({
      name: 'NotFound',
      route: '',
    });
  }

  // Build screens
  const screen: {[propertyName: string]: string} = {};
  const component: any[] = [];
  routeScreens.map(v => {
    screen[v.name] = v.route;
    component.push({
      name: v.name,
      component: screenUtil.getScreens()[v.name] || null,
    });
  });

  return {
    linking: {
      prefixes,
      config: {
        screens: screen,
      },
    },
    components: component,
  };
};

/**
 * Get all screens
 * @see layouts/screens
 */
const getScreens = () => screenList;

const screenUtil = {
  createLinking,
  getScreens,
};

export default screenUtil;